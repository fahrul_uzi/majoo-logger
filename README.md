# go-logger

## About

Logging helpers to provide standardised logging format for easy connection

Implements Uber's Zap library https://github.com/uber-go/zap