module bitbucket.org/fahrul_uzi/majoo-logger

go 1.16

require (
	github.com/google/uuid v1.3.0 // indirect
	go.uber.org/zap v1.19.1 // indirect
)
