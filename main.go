package main

import (
	"bitbucket.org/fahrul_uzi/majoo-logger/log"
	"bitbucket.org/fahrul_uzi/majoo-logger/logger"
	"time"
)

func main() {
	// initialize logger, called when app running first time
	majooLog, err := logger.NewLogger(
		logger.WithAppName("majoo-logger"),
		logger.WithLevel(logger.Level.DEBUG),
		logger.WithOutput(logger.Output.JSON),
	)

	if err != nil {
		panic("PANIC : Error Setup majooLog")
	}

	// set request time and uri, called at first function (handler/controller) caller
	majooLog.SetRequestTimeAndURI(time.Now(), "/ping")

	// Log expose, called when you want call the log
	majooLog.Info("HTTP SERVER RUNNING ON PORT 8000", log.WithRequired(log.Required{
		ResponseMessage: "Echo : Server Running on Port 8000",
		RequestParams:   "echo",
		ErrorCode:       "",
	}))

	majooLog.Error("HTTP SERVER DOESN'T RUN PROPERLY", log.WithRequired(log.Required{
		ResponseMessage: "Err : Echo need updated",
		RequestParams:   "echo.New()",
		ErrorCode:       "1010101",
	}))
}
