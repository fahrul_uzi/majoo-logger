package logger

import (
	"go.uber.org/zap"
	"time"

	"bitbucket.org/fahrul_uzi/majoo-logger/log"
)

type ILogger interface {
	Close() error
	Flush() error

	Debug(message string, options ...log.Option)
	Info(message string, options ...log.Option)
	Warn(message string, options ...log.Option)
	Error(message string, options ...log.Option)
	Panic(message string, options ...log.Option)

	SetLevel(level LevelType)
	SetRequestTimeAndURI(time time.Time, uri string)

	GetSkip() *int
}

type logger struct {
	name   *string
	logger *zap.Logger
	config *zap.Config

	requestTime *time.Time
	uri         *string

	skip *int
}
